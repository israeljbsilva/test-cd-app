# test-cd-app
App escrito em Python utilizando PyQt

# Link da aplicação no GITLAB
- https://gitlab.com/israeljbsilva/test-cd-app

# Ferramentas usadas:
- Python 3.6
- PyQt
- Git

# Ferramentas de automação
Este projeto usa o `Makefile`como ferramenta de automação.


# Set-up Virtual Environment
O seguinte comando instala a ferramenta `virtual-env`, usada para criar/gerenciar ambientes virtuais:

```bash
sudo pip3 install virtualenv 
```
Depois disso, acesse o diretório do projeto e execute `make all` para recriar o ambiente virtual e instalar as dependências.

# Executa a aplicação:
```
python main.py
```

Caso queiram rodar em ambiente local, para verificar se realmente o arquivo está sendo salvo no servidor http (test-cd-core), basta:

- Rodar locamente a aplicação `test-cd-core` conforme descrito no README do mesmo;
- Comentar a linha 9 e descomentar a linha 12 do arquivo `main.py` deste projeto, para que seja consumido a API local, onde será possível a visualização dos arquivos salvos na pasta `uploads` do projeto `test-cd-core`.