import sys
import os

from PyQt5.QtWidgets import QApplication

from views.register import RegisterWindow


os.environ['URL_API_TEST_CD_CORE'] = 'https://test-cd-core.herokuapp.com/api/v1'

# URL da api localmente
# os.environ['URL_API_TEST_CD_CORE'] = 'http://127.0.0.1:8000/api/v1'


if __name__ == '__main__':
    app = QApplication(sys.argv)
    register_window = RegisterWindow()
    register_window.show()
    sys.exit(app.exec_())
