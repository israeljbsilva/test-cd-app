# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'uic/register.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5.QtWidgets import QMainWindow, QMessageBox
from PyQt5 import QtCore, QtWidgets

from http import HTTPStatus

from views.login import LoginWindow
from views.menubar import options_menubar

from services.test_cd_core import TestCDCoreService


class RegisterWindow(QMainWindow):
    def __init__(self):
        super(RegisterWindow, self).__init__()
        self.setObjectName("Tela Inicial")
        self.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButtonRegister = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonRegister.setGeometry(QtCore.QRect(160, 420, 131, 41))
        self.pushButtonRegister.setObjectName("pushButtonRegister")
        self.lineEditUsername = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditUsername.setGeometry(QtCore.QRect(200, 190, 461, 28))
        self.lineEditUsername.setObjectName("lineEditUsername")
        self.lineEditEmail = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditEmail.setGeometry(QtCore.QRect(200, 230, 461, 28))
        self.lineEditEmail.setObjectName("lineEditEmail")
        self.lineEditPassword = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditPassword.setGeometry(QtCore.QRect(200, 270, 461, 28))
        self.lineEditPassword.setObjectName("lineEditPassword")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(110, 190, 81, 20))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(110, 230, 71, 20))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(110, 270, 81, 20))
        self.label_3.setObjectName("label_3")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(360, 60, 110, 20))
        self.label_4.setObjectName("label_4")
        self.pushButtonRegisterLogin = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonRegisterLogin.setGeometry(QtCore.QRect(490, 420, 131, 41))
        self.pushButtonRegisterLogin.setObjectName("pushButtonRegisterLogin")
        self.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(self)

        options_menubar(self)

        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 25))
        self.menubar.setObjectName("menubar")
        self.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(self)
        self.statusbar.setObjectName("statusbar")
        self.setStatusBar(self.statusbar)

        self.retranslate_ui()
        QtCore.QMetaObject.connectSlotsByName(self)

        self.pushButtonRegister.clicked.connect(self.push_button_register)
        self.pushButtonRegisterLogin.clicked.connect(self.login_window)

    def retranslate_ui(self):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("Tela Inicial", "Tela Inicial"))
        self.pushButtonRegister.setText(_translate("Tela Inicial", "Registre-se"))
        self.label.setText(_translate("Tela Inicial", "Username:"))
        self.label_2.setText(_translate("Tela Inicial", "E-mail:"))
        self.label_3.setText(_translate("Tela Inicial", "Senha:"))
        self.label_4.setText(_translate("Tela Inicial", "TEST CORE APP"))
        self.pushButtonRegisterLogin.setText(_translate("Tela Inicial", "Entrar"))

    def push_button_register(self):
        username = self.lineEditUsername.text()
        email = self.lineEditEmail.text()
        password = self.lineEditPassword.text()
        payload = {
            'username': username,
            'email': email,
            'password1': password,
            'password2': password
        }
        test_cd_core_service = TestCDCoreService()
        response = test_cd_core_service.create_user(payload)
        if response.status_code == HTTPStatus.CREATED:
            self.login_window()
        else:
            QMessageBox.about(self, "ERROR", response.text)

    def login_window(self):
        self.login = LoginWindow()
        self.login.show()
        self.close()
