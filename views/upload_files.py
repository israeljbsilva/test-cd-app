# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'uic/upload_files.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!


import json

from PyQt5.QtWidgets import QMainWindow, QFileDialog
from PyQt5 import QtCore, QtWidgets

from services.test_cd_core import TestCDCoreService
from views.menubar import options_menubar


class UploadFilesWindow(QMainWindow):

    def list_files(self):
        files_response = self.test_cd_core_service.get_files(self.user_id)
        content = json.loads(files_response.text)
        self.tableWidget.setRowCount(0)
        for row_number, row_files in enumerate(content):
            self.tableWidget.insertRow(row_number)
            for column_number, files in enumerate(row_files):
                self.tableWidget.setItem(row_number, column_number, QtWidgets.QTableWidgetItem(row_files[files]))

    def __init__(self, user_id):
        self.user_id = user_id
        self.test_cd_core_service = TestCDCoreService()
        super(UploadFilesWindow, self).__init__()
        self.setObjectName("Arquivos")
        self.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(320, 40, 161, 20))
        self.label_4.setObjectName("label_4")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(20, 155, 761, 20))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(20, 185, 761, 20))
        self.label_6.setObjectName("label_6")
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setGeometry(QtCore.QRect(20, 215, 761, 20))
        self.label_7.setObjectName("label_7")
        self.pushButtonSelectFile = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonSelectFile.setGeometry(QtCore.QRect(230, 100, 141, 28))
        self.pushButtonSelectFile.setObjectName("pushButtonSelectFile")
        self.pushButtonUploadFile = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonUploadFile.setGeometry(QtCore.QRect(430, 100, 141, 28))
        self.pushButtonUploadFile.setObjectName("pushButtonUploadFile")
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(20, 280, 761, 200))
        self.tableWidget.setRowCount(2)
        self.tableWidget.setColumnCount(2)
        self.tableWidget.setObjectName("tableWidget")
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(3, item)

        self.progressBar = QtWidgets.QProgressBar(self.centralwidget)
        self.progressBar.setGeometry(QtCore.QRect(160, 180, 620, 20))
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName("progressBar")

        self.pushButtonListFile = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonListFile.setGeometry(QtCore.QRect(330, 510, 141, 28))
        self.pushButtonListFile.setObjectName("pushButtonLoadFile")
        self.pushButtonListFile.clicked.connect(self.list_files)
        self.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(self)

        options_menubar(self)

        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 25))
        self.menubar.setObjectName("menubar")
        self.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(self)
        self.statusbar.setObjectName("statusbar")
        self.setStatusBar(self.statusbar)

        self.retranslate_ui()
        QtCore.QMetaObject.connectSlotsByName(self)

        self.pushButtonSelectFile.clicked.connect(self.open_dialog_box)
        self.pushButtonUploadFile.clicked.connect(self.upload_file)
        self.pushButtonListFile.clicked.connect(self.list_files)

    def retranslate_ui(self):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("Arquivos", "Arquivos"))
        self.label_4.setText(_translate("Arquivos", "UPLOAD DE ARQUIVOS"))
        self.pushButtonSelectFile.setText(_translate("Arquivos", "Selecionar Arquivo"))
        self.pushButtonUploadFile.setText(_translate("Arquivos", "Upload"))
        self.label_5.setText(_translate("Arquivos", "Arquivo Selecionado:"))
        self.label_6.setText(_translate("Arquivos", "Barra de Progresso:"))
        self.label_7.setText(_translate("Arquivos", "Previsão de Término:"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("Arquivos", "ID"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("Arquivos", "NOME"))
        self.pushButtonListFile.setText(_translate("Arquivos", "Atualizar Lista"))

    def open_dialog_box(self):
        self.file = QFileDialog.getOpenFileName()
        self.label_5.setText(f'Arquivo Selecionado: {self.file[0]}')

    def upload_file(self):
        response = self.test_cd_core_service.upload_file(self.file, self.user_id)
        self.label_7.setText(f'Previsão de Término: {response.elapsed.total_seconds()} segundos')
        self.completed = 0
        while self.completed < 100:
            self.completed += 0.0001
            self.progressBar.setValue(self.completed)
