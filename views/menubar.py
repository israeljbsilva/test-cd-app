from PyQt5 import QtWidgets


def options_menubar(self):
    file_menu = self.menubar.addMenu('Opções')
    quitAction = QtWidgets.QAction('Sair', self)
    quitAction.setShortcut('CTRL+Q')
    quitAction.triggered.connect(exit_app)
    file_menu.addAction(quitAction)


def exit_app(self):
    self.quit()
