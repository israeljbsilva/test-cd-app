# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'uic/login.ui'
#
# Created by: PyQt5 UI code generator 5.13.1
#
# WARNING! All changes made in this file will be lost!
import jwt

import json

from PyQt5.QtWidgets import QMainWindow, QMessageBox
from PyQt5 import QtCore, QtWidgets

from http import HTTPStatus

from views.menubar import options_menubar
from views.upload_files import UploadFilesWindow

from services.test_cd_core import TestCDCoreService


class LoginWindow(QMainWindow):
    def __init__(self):
        super(LoginWindow, self).__init__()
        self.setObjectName("Login")
        self.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(self)
        self.centralwidget.setObjectName("centralwidget")
        self.pushButtonLogin = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonLogin.setGeometry(QtCore.QRect(350, 420, 131, 41))
        self.pushButtonLogin.setObjectName("pushButtonLogin")
        self.lineEditLoginUsername = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditLoginUsername.setGeometry(QtCore.QRect(200, 190, 461, 28))
        self.lineEditLoginUsername.setObjectName("lineEditLoginUsername")
        self.lineEditLoginPassword = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditLoginPassword.setGeometry(QtCore.QRect(200, 230, 461, 28))
        self.lineEditLoginPassword.setObjectName("lineEditLoginPassword")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(110, 190, 81, 20))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(110, 230, 71, 20))
        self.label_2.setObjectName("label_2")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(390, 60, 110, 20))
        self.label_4.setObjectName("label_4")
        self.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(self)

        options_menubar(self)

        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 25))
        self.menubar.setObjectName("menubar")
        self.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(self)
        self.statusbar.setObjectName("statusbar")
        self.setStatusBar(self.statusbar)

        self.retranslate_ui()
        QtCore.QMetaObject.connectSlotsByName(self)

        self.pushButtonLogin.clicked.connect(self.push_button_login)

    def retranslate_ui(self):
        _translate = QtCore.QCoreApplication.translate
        self.setWindowTitle(_translate("Login", "Login"))
        self.pushButtonLogin.setText(_translate("Login", "Entrar"))
        self.label.setText(_translate("Login", "Username:"))
        self.label_2.setText(_translate("Login", "Senha:"))
        self.label_4.setText(_translate("Login", "Login "))

    def push_button_login(self):
        username = self.lineEditLoginUsername.text()
        password = self.lineEditLoginPassword.text()
        payload = {
            'username': username,
            'password': password
        }
        test_cd_core_service = TestCDCoreService()
        response = test_cd_core_service.login_user(payload)
        if response.status_code == HTTPStatus.OK:
            token = json.loads(response.text)['access']
            payload = (jwt.decode(token, verify=False))
            user_id = payload.get('user_id')
            self.upload_files = UploadFilesWindow(user_id)
            self.upload_files.show()
            self.close()
        else:
            QMessageBox.about(self, "ERROR", response.text)
