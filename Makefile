PIP := pip install -r

PROJECT_NAME := test-cd-app
PYTHON_VERSION := 3.6.6
VENV_NAME := $(PROJECT_NAME)-$(PYTHON_VERSION)

# Environment setup
.pip:
	pip install pip --upgrade

setup: .pip
	$(PIP) requirements.txt

.create-venv:
	virtualenv venv
	source venv/bin/activate

create-venv: .create-venv setup

all: create-venv setup
