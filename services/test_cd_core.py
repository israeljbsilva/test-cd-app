import os

import requests


class TestCDCoreService:

    def __init__(self):
        self._url_api_test_cd_core = os.environ.get('URL_API_TEST_CD_CORE')

    def create_user(self, payload):
        response = requests.post(url=f'{self._url_api_test_cd_core}/user/create', data=payload)
        return response

    def login_user(self, payload):
        response = requests.post(url=f'{self._url_api_test_cd_core}/user/auth', data=payload)
        return response

    def upload_file(self, file, user_id):
        path = file[0]
        read_file = {'upload_file': open(path, 'rb')}
        response = requests.post(url=f'{self._url_api_test_cd_core}/file/upload', files=read_file,
                                 headers={'user': str(user_id),
                                          'Content-Disposition': f'attachment;filename={path.split("/")[-1]}'})
        return response

    def get_files(self, user_id):
        response = requests.get(url=f'{self._url_api_test_cd_core}/file?user={user_id}')
        return response
